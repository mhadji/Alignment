/** @file MMapVector.cpp
 *
 * @date 2017-02-11
 * @author Manuel Schiller      <Manuel.Schiller@glasgow.ac.uk>
 *
 * @brief implementation details of the MMapVector class
 */

#include "Pi0Calibration/MMapVector.h"

namespace MMapVector_impl {
    /// implementation of 64 bit FNV1a hash for checksums etc
    uint64_t fnv1a(const void* data, uint64_t len) noexcept
    {
        const uint8_t *begin = static_cast<const uint8_t*>(data);
        const auto end = begin + len;
        uint64_t retVal = 14695981039346656037ull;
        while (end != begin) {
            retVal ^= *begin++;
            retVal *= 1099511628211ull;
        }
        return retVal;
    }
    namespace {
        /// find out page size of system
        uint64_t getPageSize()
        {
            long pgsz = sysconf(_SC_PAGESIZE);
            if (-1 == pgsz) throw std::system_error(
                    errno, std::generic_category());
            // must be a power of 2, and between 512 bytes and 4 MiB inclusive
            if (pgsz < 512 || pgsz > (4 << 20) || (pgsz & (pgsz - 1)))
                throw std::out_of_range("got totally unexpected page size");
            return pgsz;
        }
    }
    /// return cached system page size
    uint64_t pagesize()
    {
        static uint64_t s_pagesize = getPageSize();
        return s_pagesize;
    }

    Mapping::Mapping(int fd, uint64_t offset, uint64_t len,
            Mapping::Protection prot) : Mapping()
    {
        while (MAP_FAILED == (m_data = mmap(nullptr, len,
                        static_cast<int>(prot), MAP_SHARED, fd, offset))) {
            if (EINTR == errno) continue;
            throw std::system_error(errno, std::generic_category());
        }
        m_len = len;
    }
    Mapping::~Mapping()
    { if (MAP_FAILED != m_data) munmap(m_data, m_len); }
    Mapping& Mapping::operator=(Mapping&& other)
    {
        if (&other != this) {
            m_data = other.m_data;
            m_len = other.m_len;
            other.m_data = MAP_FAILED;
            other.m_len = 0;
        }
        return *this;
    }
    void Mapping::resize(uint64_t newsz)
    {
        void* newdata = MAP_FAILED;
        if (0 != newsz) {
            while (MAP_FAILED ==
                    (newdata = mremap(m_data, m_len,
                                      newsz, MREMAP_MAYMOVE))) {
                if (EINTR == errno) continue;
                throw std::system_error(
                        errno, std::generic_category());
            }
        } else {
            while (0 != munmap(m_data, m_len)) {
                if (EINTR == errno) continue;
                throw std::system_error(
                        errno, std::generic_category());
            }
            newdata = MAP_FAILED;
        }
        m_len = newsz;
        m_data = newdata;
    }
    void Mapping::msync()
    {
        while (-1 == ::msync(m_data, m_len, MS_ASYNC)) {
            if (EINTR == errno) continue;
            throw std::system_error(
                    errno, std::generic_category());
        }
    }
    File::File(const char* fname, File::Flags flags) : File()
    {
        m_flags = flags;
        char tmpname[] = "/tmp/MMapVector.XXXXXX";
        if (nullptr == fname && Flags::ReadWriteTemp == flags) {
            // open a temporary file name for reading and writing...
            while (-1 == (m_fd = mkstemp(tmpname))) {
                if (EINTR == errno) continue;
                throw std::system_error(errno, std::generic_category());
            }
        } else {
            // open file specified by fname (potentially creating it
            // if it did not exist)
            int fl = (Flags::ReadOnly >= flags) ? O_RDONLY : O_RDWR;
            if (Flags::ReadWriteCreate == flags ||
                    Flags::ReadWriteTemp == flags)
                fl |= O_CREAT;
            fl |= O_LARGEFILE;
            while (-1 == (m_fd = open(fname, fl, S_IRUSR | S_IWUSR))) {
                if (EINTR == errno) continue;
                throw std::system_error(errno, std::generic_category());
            }
        }
        if (Flags::ReadWriteTemp == flags) {
            // unlink fname, if it's supposed to be a temporary
            // file (so the OS will free up the space when the
            // process dies...)
            while (-1 == unlink(fname ? fname : tmpname)) {
                if (EINTR == errno) continue;
                throw std::system_error(errno, std::generic_category());
            }
        }
    }
    File::File(const File& other) : File()
    {
        while (-1 == (m_fd = dup(other.m_fd))) {
            if (EINTR == errno) continue;
            throw std::system_error(errno, std::generic_category());
        }
    }
    File& File::operator=(File&& other)
    {
        if (&other != this) {
            if (-1 != m_fd) close(m_fd);
            m_fd = std::move(other.m_fd);
            other.m_fd = -1;
        }
        return *this;
    }
    File& File::operator=(const File& other)
    {
        if (&other != this) {
            if (-1 != m_fd) close(m_fd);
            while (-1 == (m_fd = dup(other.m_fd))) {
                if (EINTR == errno) continue;
                throw std::system_error(errno, std::generic_category());
            }
        }
        return *this;
    }
    void File::read(void* buf, std::size_t count)
    {
        char* p = static_cast<char*>(buf);
        std::size_t xfer = 0;
        while (xfer < count) {
            auto rc = ::read(m_fd, &p[xfer], count - xfer);
            if (-1 == rc) {
                if (EINTR == errno) continue;
                throw std::system_error(errno, std::generic_category());
            }
            xfer += rc;
        }
    }
    void File::write(const void* buf, std::size_t count)
    {
        const char* p = static_cast<const char*>(buf);
        std::size_t xfer = 0;
        while (xfer < count) {
            auto rc = ::write(m_fd, &p[xfer], count - xfer);
            if (-1 == rc) {
                if (EINTR == errno) continue;
                throw std::system_error(errno, std::generic_category());
            }
            xfer += rc;
        }
    }
    uint64_t File::seek(int64_t offset, File::Whence w)
    {
        static_assert(sizeof(uint64_t) == sizeof(off_t),
                "file offset type off_t needs to be a 64 bit type");
        int64_t rc;
        while (-1 == (rc = lseek(m_fd, offset,
                        static_cast<int>(w)))) {
            if (EINTR == errno) continue;
            throw std::system_error(errno, std::generic_category());
        }
        return rc;
    }
    void File::truncate(int64_t offset)
    {
        while (-1 == ftruncate(m_fd, offset)) {
            if (EINTR == errno) continue;
            throw std::system_error(errno, std::generic_category());
        }
    }
}

// vim: sw=4:tw=78:ft=cpp:et
