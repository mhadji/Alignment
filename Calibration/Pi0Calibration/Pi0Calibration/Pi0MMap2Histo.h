/*
 * =====================================================================================
 *
 *       Filename:  Pi0MMap2Histo.h
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  02/27/2017 05:01:10 PM
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Zhirui Xu (), Zhirui.Xu@cern.ch
 *   Organization:  
 *
 * =====================================================================================
 */
#ifndef CALIBRATION_PI0CALIBRATION_PI0MMAP2HISTO_H
#define CALIBRATION_PI0CALIBRATION_PI0MMAP2HISTO_H

#include <string>
#include <map>
#include <vector>
#include <utility>

#include "GaudiAlg/GaudiAlgorithm.h"


class Pi0MMap2Histo : public GaudiAlgorithm {
    public:
	Pi0MMap2Histo( const std::string& name, ISvcLocator* pSvcLocator );
	virtual ~Pi0MMap2Histo();

        virtual StatusCode initialize() override;
        virtual StatusCode execute() override;
        virtual StatusCode finalize() override;

    private:
	std::vector<std::string> m_filenames;
	std::string m_outputDir;
	std::string m_outputName;
        unsigned int m_nworker;
	std::string m_lambdaFileName;

        std::vector<unsigned int > m_indices;

};

#endif
