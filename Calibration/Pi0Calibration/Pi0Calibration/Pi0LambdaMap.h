/*
 * =====================================================================================
 *
 *       Filename:  Pi0LambdaMap.h
 *
 *    Description:  :
 *
 *        Version:  1.0
 *        Created:  12/06/2016 02:25:57 PM
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Zhirui Xu (LAPP-CNRS), Zhirui.Xu@cern.ch
 *   Organization:  
 *
 * =====================================================================================
 */


#ifndef CALIBRATION_PI0CALIBRATION_PI0LAMBDAMAP_H
#define CALIBRATION_PI0CALIBRATION_PI0LAMBDAMAP_H 1

#include <string>
#include <map>
#include <vector>
#include <utility>

namespace Calibration {
    namespace Pi0Calibration {
	class Pi0LambdaMap {
          public:
	      Pi0LambdaMap( const std::vector<unsigned int>& indices);
	      virtual ~Pi0LambdaMap(){}

	      void loadMapFromFile( const std::string& filename );
	      std::map< unsigned int, std::pair<double,double> > lambdas() { return m_lambdas; }

	      std::pair<double,double> get_lambda(const unsigned int index) const;
              void setLambda(const unsigned int index, const std::pair<double,double>& lambda);
	      void saveMapToFile( const std::string& filename );

	  private:
	      std::map< unsigned int, std::pair<double,double> > m_lambdas;

	};
    }//namespace Pi0Calibration
}//namespace Calibration

#endif
