#!/bin/python
import os
outputdir = os.path.join( "/home3/xu/worksapce/CALO/pi0calibration/data2016/TEST" )
datatype = 'TH2D'
if datatype=='ROOT':
    tuplefile = "/gpfs/LAPP-DATA/lhcb/xu/Second_data_2016_pi0.root"
if datatype=='MMAP':
    tuplefile = "/gpfs/LAPP-DATA/lhcb/xu/Second_data_2016_pi0.MMap"
if datatype=='TH2D':
    tuplefile = "/gpfs/LAPP-DATA/lhcb/xu/Second_Worker1.root"

if '__main__' == __name__ :
    cmd = "~/cmtuser/AlignmentDev_v11r1p1/run python do_calibration.py --filename %s --filetype %s "%( tuplefile, datatype )
    cmd+= "--outdir %s "%outputdir
    cmd+= "--indir %s  "%outputdir
    cmd+= "--year 2016 "

    import subprocess
    for iteration in xrange(1,7+1):
        cmd+= "--iteration %s "%iteration
        subprocess.call( cmd , shell=True) 
