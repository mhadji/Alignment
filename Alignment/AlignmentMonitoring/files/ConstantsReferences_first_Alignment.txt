# A file to define the ranges within the constants can change to raise specific alarms
# 0 - Normal - x1 - Warning - x2 - Severe

# Global
Tracker\..*	0.00001	0.001

# Velo Global
VeloSystem\..*	0.000015	0.00001
Velo\..*	0.000015	0.00001
Velo(Left|Right)\.T(x|y)	0.0015	0.01
Velo(Left|Right)\.Tz	0.005	0.01
Velo(Left|Right)\.R(x|y)	0.000004	0.000025
Velo(Left|Right)\.Rz	0.000030	0.0001

# Velo Modules
.*?Module[0-9]{2}\.T(x|y)	0.002	0.01
.*?Module[0-9]{2}\.Rz	0.0001	0.0003
.*?Module[0-9]{2}\.Tz	0.00001	0.00002
.*?Module[0-9]{2}\.R(x|y)	0.000001	0.00001
.*?Detector([0-3][0-9]|4[0-1])-0[0-1]\..*	0.0000015	0.000001
Velo/Velo(Left|Right)/Module([0-3][0-9]|4[0-1])/RPhiPair.*/Detector-0[0-1]\..*	0.0000015	0.000001
# pile-up module
.*?ModulePU0[0-3]\..*	0.0000015	0.000001
.*?DetectorPU0[0-3]-00\..*	0.0000015	0.000001



# IT Global
ITSystem\..*	0.00015	0.0001
ITT(1|2|3)\..*	0.00015	0.0001
ITT.*Box\.R(x|y)	0.00015	0.0001
ITT.*Box\.Tx	0.08	5.0   
ITT.*Box\.Ty	0.008	0.05  
ITT.*Box\.Tz	0.2	5.0   
ITT.*Box\.Rz	0.0008	0.008 
# elements name
IT\..*	0.00015	0.0001
IT/Station(1|2|3)\..*	0.00015	0.0001
IT/Station.*Box\.R(x|y)	0.00015	0.0001
IT/Station.*Box\.Tx	0.08	5.0   
IT/Station.*Box\.Ty	0.008	0.05  
IT/Station.*Box\.Tz	0.2	5.0   
IT/Station.*Box\.Rz	0.0008	0.008 

# IT Modules
ITT.*Layer.*Ladder.*	0.00015	0.0001
ITT.*Layer.*	0.00015	0.0001
# elements name
IT/.*Layer.*Ladder.*	0.00015	0.0001
IT/.*Layer.*	0.00015	0.0001


# TT Global
TTSystem\..*	0.00015	0.0001
TTa\..*	0.00015	0.0001
TTb\..*	0.00015	0.0001
TT.*Layer\.Tx	0.00015	0.0001
TT.*Layer\.Ty	0.0006	0.0005
TT.*Layer\.Tz	0.1	1.0
TT.*Layer\.R.*	0.00015	0.0001
# elements name
TT\..*	0.00015	0.0001
TT/TT(a|b)\..*	0.00015	0.0001
TT/TT.*Layer\.Tx	0.00015	0.0001
TT/TT.*Layer\.Ty	0.0006	0.0005
TT/TT.*Layer\.Tz	0.1	1.0
TT/TT.*Layer\.R.*	0.00015	0.0001

# TT Modules
*?TT.*Layer.*?R.*Module.*\.Tx	0.1	0.4
.*?TT.*Layer.*?R.*Module.*\.Ty	0.0006	0.0005
.*?TT.*Layer.*?R.*Module.*\.Tz	0.00015	0.0001
.*?TT.*Layer.*?R.*Module.*\.R(x|y)	0.00015	0.0001
.*?TT.*Layer.*?R.*Module.*\.Rz	0.00015	0.0001



# OT Global
OTSystem\..*	0.00015	0.0001
T(1|2|3)\..*	0.00015	0.0001
T(1|2|3)(X1|U|V|X2)\..*	0.0025	0.002
T(1|2|3)(X1|U|V|X2)Q(0|1|2|3)\.Tx	0.1	1.0   
T(1|2|3)(X1|U|V|X2)Q(0|1|2|3)\.Tz	0.1	1.0   
T(1|2|3)(X1|U|V|X2)Q(0|1|2|3)\.Rz	0.0006	0.0005
T(1|2|3)(X1|U|V|X2)Q(0|1|2|3)\.Ty	0.0006	0.0005
T(1|2|3)(X1|U|V|X2)Q(0|1|2|3)\.(Rx|Ry)	0.0006	0.0005

# elements name
OT\..*	0.00015	0.0001
OT/T(1|2|3)(X1|U|V|X2)(A|C)Side\.Tx	0.1	1.0   
OT/T(1|2|3)(X1|U|V|X2)(A|C)Side\.Tz	0.1	1.0   
OT/T(1|2|3)(X1|U|V|X2)(A|C)Side\.Rz	0.0006	0.0005
OT/T(1|2|3)(X1|U|V|X2)(A|C)Side\.Ty	0.0006	0.0005
OT/T(1|2|3)(X1|U|V|X2)(A|C)Side\.(Rx|Ry)	0.0006	0.0005
OT/T(1|2|3)(X1U|VX2)(A|C)Side\.Tx	0.1	1.0   
OT/T(1|2|3)(X1U|VX2)(A|C)Side\.Tz	0.1	1.0   
OT/T(1|2|3)(X1U|VX2)(A|C)Side\.Rz	0.0006	0.0005
OT/T(1|2|3)(X1U|VX2)(A|C)Side\.Ty	0.0006	0.0005
OT/T(1|2|3)(X1U|VX2)(A|C)Side\.(Rx|Ry)	0.0006	0.0005
OT/T(1|2|3)(X1U|VX2)\.Tx	0.1	1.0   
OT/T(1|2|3)(X1U|VX2)\.Tz	0.1	1.0   
OT/T(1|2|3)(X1U|VX2)\.Rz	0.0006	0.0005
OT/T(1|2|3)(X1U|VX2)\.Ty	0.0006	0.0005
OT/T(1|2|3)(X1U|VX2)\.(Rx|Ry)	0.0006	0.0005

# OT Modules
T(1|2|3)(X1|U|V|X2)Q(0|1|2|3)M(1|2)\.Tx	0.15	0.6   
T(1|2|3)(X1|U|V|X2)Q(0|1|2|3)M[3-9].*\.Tx	0.1	0.6   
T(1|2|3)(X1|U|V|X2)Q(0|1|2|3)M.*\.Ty	0.0006	0.0005
T(1|2|3)(X1|U|V|X2)Q(0|1|2|3)M.*\.Tz	0.00015 0.0001
T(1|2|3)(X1|U|V|X2)Q(0|1|2|3)M.*\.R(x|y)	0.00015	0.0001
T(1|2|3)(X1|U|V|X2)Q(0|1|2|3)M.*\.Rz	0.00015	0.0001
# elements name
T(1|2|3)(X1|U|V|X2)Q.*M(1|2)\.Tx	0.15	0.6   
T(1|2|3)(X1|U|V|X2)Q.*M[3-9].*\.Tx	0.1	0.6   
T(1|2|3)(X1|U|V|X2)Q.*M.*\.Ty	0.0006	0.0005
T(1|2|3)(X1|U|V|X2)Q.*M.*\.Tz	0.00015 0.0001
T(1|2|3)(X1|U|V|X2)Q.*M.*\.R(x|y)	0.00015	0.0001
T(1|2|3)(X1|U|V|X2)Q.*M.*\.Rz	0.00015	0.0001

# Muon chambers
MuonSystem\..*	0.00015	0.0001
M.Station\..*	0.00015	0.0001
M..Side\..*	1.5	1.
