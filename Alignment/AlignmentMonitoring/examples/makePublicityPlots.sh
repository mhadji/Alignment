#!/usr/bin/env bash

export 'firstRunVelo'='191811' 
export 'firstRunTracker'='192119' 
export 'firstRunMuon'='192715' 
export 'beginVelo'='24/05/2017' 
export 'beginTracker'='27/05/2017' 
export 'beginMuon'='05/06/2017' # After manual intervention
export 'lastRun'='500000' 
export 'today'=$(date +"%d/%m/%Y")
export 'outputFolder'='publicityPlots'

while getopts "o:" opt; do
    case "$opt" in
    o)  outputFolder=$OPTARG
        ;;
    esac
done

rm -r $outputFolder
mkdir -p tmp/AlignmentMonitoring

# Velo Tx Ty
$ALIGNMENTMONITORINGROOT/scripts/trendPlots.py -stl  -a VeloLeft.Tx VeloLeft.Ty \
		-r $firstRunVelo $lastRun \
		--labels VeloLeft.Tx:x-translation VeloLeft.Ty:y-translation --legTextSize 0.05 \
		--legTransparent --legTextSize 0.05 --legPosition .65 0.8 .9 .9 \
		--stickers "{(.2,.7,0.45,0.9, 0.05): ['#scale[1.5]{LHCb VELO}', '#scale[1.3]{Preliminary}'], (.65,.2,0.9,0.25,0.05): '$beginVelo - $today',  (.25,.2,0.4,0.25,0.05): 'Empty markers = no update'}" \
		-o tmp/AlignmentMonitoring/Velo_stability_Txy

$ALIGNMENTMONITORINGROOT/scripts/trendPlots.py -xsl  -a VeloLeft.Tx VeloLeft.Ty \
		-r $firstRunVelo $lastRun \
		--labels VeloLeft.Tx:x-translation VeloLeft.Ty:y-translation --legTextSize 0.05 \
		--legTransparent --legTextSize 0.05 --legPosition .65 0.8 .9 .9 \
		--stickers "{(.2,.7,0.45,0.9, 0.05): ['#scale[1.5]{LHCb VELO}', '#scale[1.3]{Preliminary}'], (.65,.2,0.9,0.25,0.05): '$beginVelo - $today',  (.25,.2,0.4,0.25,0.05): 'Empty markers = no update'}" \
		-o tmp/AlignmentMonitoring/Velo_trend_Txy

$ALIGNMENTMONITORINGROOT/scripts/trendPlots.py -nstl  -a VeloLeft.Tx VeloLeft.Ty \
		-r $firstRunVelo $lastRun \
		--labels VeloLeft.Tx:x-translation VeloLeft.Ty:y-translation --legTextSize 0.05 \
		--legTransparent --legTextSize 0.05 --legPosition .65 0.8 .9 .9 \
		--stickers "{(.2,.7,0.45,0.9, 0.05): ['#scale[1.5]{LHCb VELO}', '#scale[1.3]{Preliminary}'], (.65,.2,0.9,0.25,0.05): '$beginVelo - $today'}" \
		-o tmp/AlignmentMonitoring/Velo_stability_Txy_fd

$ALIGNMENTMONITORINGROOT/scripts/trendPlots.py -nxsl  -a VeloLeft.Tx VeloLeft.Ty \
		-r $firstRunVelo $lastRun \
		--labels VeloLeft.Tx:x-translation VeloLeft.Ty:y-translation --legTextSize 0.05 \
		--legTransparent --legTextSize 0.05 --legPosition .65 0.8 .9 .9 \
		--stickers "{(.2,.7,0.45,0.9, 0.05): ['#scale[1.5]{LHCb VELO}', '#scale[1.3]{Preliminary}'], (.65,.2,0.9,0.25,0.05): '$beginVelo - $today'}" \
		-o tmp/AlignmentMonitoring/Velo_trend_Txy_fd

# Muon Tx Ty

$ALIGNMENTMONITORINGROOT/scripts/trendPlots.py -xsl -a M1ASide.Tx  M2ASide.Tx M3ASide.Tx M4ASide.Tx M5ASide.Tx \
		-r $firstRunMuon $lastRun  --yLabel "#DeltaX Variation [mm]"\
		--labels M1ASide.Tx:M1 M2ASide.Tx:M2 M3ASide.Tx:M3 M4ASide.Tx:M4 M5ASide.Tx:M5 \
		--legTransparent --legTextSize 0.05 --legPosition 0.7 0.7 0.9 0.9 \
		--stickers "{(.2,.7,0.45,0.9, 0.05): ['#scale[1.5]{LHCb Muon A-side}', '#scale[1.3]{Preliminary}'], (.65,.2,0.9,0.25,0.05): '$beginMuon - $today'}" \
		--activity Muon -o tmp/AlignmentMonitoring/MuonA_stability_Tx

$ALIGNMENTMONITORINGROOT/scripts/trendPlots.py -xsl -a M1ASide.Ty  M2ASide.Ty M3ASide.Ty M4ASide.Ty M5ASide.Ty \
		-r $firstRunMuon $lastRun  --yLabel "#DeltaY Variation [mm]"\
		--labels M1ASide.Ty:M1 M2ASide.Ty:M2 M3ASide.Ty:M3 M4ASide.Ty:M4 M5ASide.Ty:M5 \
		--legTransparent --legTextSize 0.05 --legPosition 0.7 0.7 0.9 0.9 \
		--stickers "{(.2,.7,0.45,0.9, 0.05): ['#scale[1.5]{LHCb Muon A-side}', '#scale[1.3]{Preliminary}'], (.65,.2,0.9,0.25,0.05): '$beginMuon - $today'}" \
		--activity Muon -o tmp/AlignmentMonitoring/MuonA_stability_Ty

$ALIGNMENTMONITORINGROOT/scripts/trendPlots.py -xsl -a M1CSide.Tx  M2CSide.Tx M3CSide.Tx M4CSide.Tx M5CSide.Tx \
		-r $firstRunMuon $lastRun  --yLabel "#DeltaX Variation [mm]"\
		--labels M1CSide.Tx:M1 M2CSide.Tx:M2 M3CSide.Tx:M3 M4CSide.Tx:M4 M5CSide.Tx:M5 \
		--legTransparent --legTextSize 0.05 --legPosition 0.7 0.7 0.9 0.9 \
		--stickers "{(.2,.7,0.45,0.9, 0.05): ['#scale[1.5]{LHCb Muon C-side}', '#scale[1.3]{Preliminary}'], (.65,.2,0.9,0.25,0.05): '$beginMuon - $today'}" \
		--activity Muon -o tmp/AlignmentMonitoring/MuonC_stability_Tx

$ALIGNMENTMONITORINGROOT/scripts/trendPlots.py -xsl -a M1CSide.Ty  M2CSide.Ty M3CSide.Ty M4CSide.Ty M5CSide.Ty \
		-r $firstRunMuon $lastRun  --yLabel "#DeltaY Variation [mm]"\
		--labels M1CSide.Ty:M1 M2CSide.Ty:M2 M3CSide.Ty:M3 M4CSide.Ty:M4 M5CSide.Ty:M5 \
		--legTransparent --legTextSize 0.05 --legPosition 0.7 0.7 0.9 0.9 \
		--stickers "{(.2,.7,0.45,0.9, 0.05): ['#scale[1.5]{LHCb Muon C-side}', '#scale[1.3]{Preliminary}'], (.65,.2,0.9,0.25,0.05): '$beginMuon - $today'}" \
		--activity Muon -o tmp/AlignmentMonitoring/MuonC_stability_Ty

$ALIGNMENTMONITORINGROOT/scripts/trendPlots.py -nxsl -a M1ASide.Tx  M2ASide.Tx M3ASide.Tx M4ASide.Tx M5ASide.Tx \
		-r $firstRunMuon $lastRun  --yLabel "#DeltaX Variation [mm]"\
		--labels M1ASide.Tx:M1 M2ASide.Tx:M2 M3ASide.Tx:M3 M4ASide.Tx:M4 M5ASide.Tx:M5 \
		--legTransparent --legTextSize 0.05 --legPosition 0.7 0.7 0.9 0.9 \
		--stickers "{(.2,.7,0.45,0.9, 0.05): ['#scale[1.5]{LHCb Muon A-side}', '#scale[1.3]{Preliminary}'], (.65,.2,0.9,0.25,0.05): '$beginMuon - $today'}" \
		--activity Muon -o tmp/AlignmentMonitoring/MuonA_stability_Tx_fd

$ALIGNMENTMONITORINGROOT/scripts/trendPlots.py -nxsl -a M1ASide.Ty  M2ASide.Ty M3ASide.Ty M4ASide.Ty M5ASide.Ty \
		-r $firstRunMuon $lastRun  --yLabel "#DeltaY Variation [mm]"\
		--labels M1ASide.Ty:M1 M2ASide.Ty:M2 M3ASide.Ty:M3 M4ASide.Ty:M4 M5ASide.Ty:M5 \
		--legTransparent --legTextSize 0.05 --legPosition 0.7 0.7 0.9 0.9 \
		--stickers "{(.2,.7,0.45,0.9, 0.05): ['#scale[1.5]{LHCb Muon A-side}', '#scale[1.3]{Preliminary}'], (.65,.2,0.9,0.25,0.05): '$beginMuon - $today'}" \
		--activity Muon -o tmp/AlignmentMonitoring/MuonA_stability_Ty_fd

$ALIGNMENTMONITORINGROOT/scripts/trendPlots.py -nxsl -a M1CSide.Tx  M2CSide.Tx M3CSide.Tx M4CSide.Tx M5CSide.Tx \
		-r $firstRunMuon $lastRun  --yLabel "#DeltaX Variation [mm]"\
		--labels M1CSide.Tx:M1 M2CSide.Tx:M2 M3CSide.Tx:M3 M4CSide.Tx:M4 M5CSide.Tx:M5 \
		--legTransparent --legTextSize 0.05 --legPosition 0.7 0.7 0.9 0.9 \
		--stickers "{(.2,.7,0.45,0.9, 0.05): ['#scale[1.5]{LHCb Muon C-side}', '#scale[1.3]{Preliminary}'], (.65,.2,0.9,0.25,0.05): '$beginMuon - $today'}" \
		--activity Muon -o tmp/AlignmentMonitoring/MuonC_stability_Tx_fd

$ALIGNMENTMONITORINGROOT/scripts/trendPlots.py -nxsl -a M1CSide.Ty  M2CSide.Ty M3CSide.Ty M4CSide.Ty M5CSide.Ty \
		-r $firstRunMuon $lastRun  --yLabel "#DeltaY Variation [mm]"\
		--labels M1CSide.Ty:M1 M2CSide.Ty:M2 M3CSide.Ty:M3 M4CSide.Ty:M4 M5CSide.Ty:M5 \
		--legTransparent --legTextSize 0.05 --legPosition 0.7 0.7 0.9 0.9 \
		--stickers "{(.2,.7,0.45,0.9, 0.05): ['#scale[1.5]{LHCb Muon C-side}', '#scale[1.3]{Preliminary}'], (.65,.2,0.9,0.25,0.05): '$beginMuon - $today'}" \
		--activity Muon -o tmp/AlignmentMonitoring/MuonC_stability_Ty_fd

# Tracker Tx Tz

$ALIGNMENTMONITORINGROOT/scripts/trendPlots.py -stl -a ITT1ASideBox.Tx ITT1CSideBox.Tx ITT1TopBox.Tx ITT1BottomBox.Tx \
		-r $firstRunTracker $lastRun \
		--rangeY -0.2 0.2 --yLabel "#DeltaX Variation [mm]"\
		--labels ITT1ASideBox.Tx:"IT1 ASide" ITT1CSideBox.Tx:"IT1 CSide" ITT1TopBox.Tx:"IT1 Top" ITT1BottomBox.Tx:"IT1 Bottom" \
		--legTransparent --legTextSize 0.05 --legPosition 0.7 0.73 0.9 0.93 \
		--stickers "{(.2,.7,0.45,0.9, 0.05): ['#scale[1.5]{LHCb Tracker}', '#scale[1.3]{Preliminary}'], (.65,.2,0.9,0.25,0.05): '$beginTracker - $today',  (.25,.2,0.4,0.25,0.05): 'Empty markers = no update'}" \
		--activity Tracker -o tmp/AlignmentMonitoring/Tracker_stability_Tx

$ALIGNMENTMONITORINGROOT/scripts/trendPlots.py -stl -a ITT1ASideBox.Tz ITT1CSideBox.Tz ITT1TopBox.Tz ITT1BottomBox.Tz \
		-r $firstRunTracker $lastRun \
		--rangeY -1 1 --yLabel "#DeltaZ Variation [mm]"\
		--labels ITT1ASideBox.Tz:"IT1 ASide" ITT1CSideBox.Tz:"IT1 CSide" ITT1TopBox.Tz:"IT1 Top" ITT1BottomBox.Tz:"IT1 Bottom" \
		--legTransparent --legTextSize 0.05 --legPosition 0.7 0.73 0.9 0.93 \
		--stickers "{(.2,.7,0.45,0.9, 0.05): ['#scale[1.5]{LHCb Tracker}', '#scale[1.3]{Preliminary}'], (.65,.2,0.9,0.25,0.05): '$beginTracker - $today',  (.25,.2,0.4,0.25,0.05): 'Empty markers = no update'}" \
		--activity Tracker -o tmp/AlignmentMonitoring/Tracker_stability_Tz

$ALIGNMENTMONITORINGROOT/scripts/trendPlots.py -nstl -a ITT1ASideBox.Tx ITT1CSideBox.Tx ITT1TopBox.Tx ITT1BottomBox.Tx \
		-r $firstRunTracker $lastRun \
		--rangeY -0.2 0.2 --yLabel "#DeltaX Variation [mm]"\
		--labels ITT1ASideBox.Tx:"IT1 ASide" ITT1CSideBox.Tx:"IT1 CSide" ITT1TopBox.Tx:"IT1 Top" ITT1BottomBox.Tx:"IT1 Bottom" \
		--legTransparent --legTextSize 0.05 --legPosition 0.7 0.73 0.9 0.93 \
		--stickers "{(.2,.7,0.45,0.9, 0.05): ['#scale[1.5]{LHCb Tracker}', '#scale[1.3]{Preliminary}'], (.65,.2,0.9,0.25,0.05): '$beginTracker - $today'}" \
		--activity Tracker -o tmp/AlignmentMonitoring/Tracker_stability_Tx_fd

$ALIGNMENTMONITORINGROOT/scripts/trendPlots.py -nstl -a ITT1ASideBox.Tz ITT1CSideBox.Tz ITT1TopBox.Tz ITT1BottomBox.Tz \
		-r $firstRunTracker $lastRun \
		--rangeY -1 1 --yLabel "#DeltaZ Variation [mm]"\
		--labels ITT1ASideBox.Tz:"IT1 ASide" ITT1CSideBox.Tz:"IT1 CSide" ITT1TopBox.Tz:"IT1 Top" ITT1BottomBox.Tz:"IT1 Bottom" \
		--legTransparent --legTextSize 0.05 --legPosition 0.7 0.73 0.9 0.93 \
		--stickers "{(.2,.7,0.45,0.9, 0.05): ['#scale[1.5]{LHCb Tracker}', '#scale[1.3]{Preliminary}'], (.65,.2,0.9,0.25,0.05): '$beginTracker - $today'}" \
		--activity Tracker -o tmp/AlignmentMonitoring/Tracker_stability_Tz_fd


mkdir $outputFolder
cp tmp/AlignmentMonitoring/*.pdf $outputFolder
rm -r tmp/AlignmentMonitoring

