#!/usr/bin/env python

##########################
###   Options parser   ###
if __name__ == '__main__':
    import argparse
    parser = argparse.ArgumentParser(description ="Macro to make plots to monitor automatic Muon alignment")
    parser.add_argument('-r','--run', help='run number, default is the last one')
    parser.add_argument('--alignlog', help='location of alignlog.txt, default is guessed by run number')
    parser.add_argument('--histoFiles', help='location of histograms files, expect two paths, old ad new align histos, default is guessed by run number', nargs=2)
    parser.add_argument('-o','--outFile',help='output file name')
    parser.add_argument('--online',help='Setup monitoring job to send the histograms to the presenter', action='store_true')
    args = parser.parse_args()

##########################

import os, sys, fnmatch, re
import ROOT as r
from GaudiPython import gbl
from AlignmentOutputParser.AlignOutput import *
from AlignmentMonitoring.MultiPlot import MultiPlot
from AlignmentMonitoring.OnlineUtils import findLastRun, findAlignlog, findHistos
from AlignmentMonitoring.RootUtils import getExpression, getDofDeltas, getDofDeltaConvergence, makeGraph, makeHisto, getDrawnCanvas, makeGraphHisto
import AlignmentMonitoring.RootUtils
from moniPlots import plotsCompare


AlMon = gbl.Alignment.AlignmentMonitoring

r.gROOT.SetBatch(True)

activity = 'Muon'

def getDofDeltaConvergence(aout, dof='Tx', alignable='Velo/VeloLeft'):
    '''
    redefine to have output in mm and mrad
    '''
    res = AlignmentMonitoring.RootUtils.getDofDeltaConvergence(aout, dof, alignable)
    return [i/1000. for i in res[0]], [i/1000. for i in res[1]]


   

if __name__ == '__main__':

    if not (args.alignlog and args.histoFiles):
        run = args.run if args.run else findLastRun(activity)
    alignlog = args.alignlog if args.alignlog else findAlignlog(activity, run)
    # files_histo = {'old': args.histoFiles[0], 'new': args.histoFiles[1]} if args.histoFiles else findHistos(activity, run)

    if args.online:
        #################################################################
        # Initialize monitoring Job to send plots to the presenter
        #################################################################
        from Configurables import MonitoringJob
        from Monitoring.MonitoringJob import start
        mj = MonitoringJob()
        # JobName should be the same as the saver
        mj.JobName = "MoniOnlineAlig"
        mj.Sender = True
        mj.Saver = False
        # Start our main job
        gaudi, monSvc = start()
        folder_name = 'MoniOnlineAligMuon'
        #################################################################

        
    # read alignlog
    aout = AlignOutput(alignlog)
    aout.Parse()
    
    c = r.TCanvas('c', 'c')
    outputFile_name = args.outFile if args.outFile else 'MoniPlots.pdf'
    c.Print(outputFile_name+'[')

    # Plots compare before-after
    # plotsCompare(Pages, files_histo, outputFile_name)

    mps = []
    
    for expression, title in [ ('NormalisedChi2Change', '#Delta #chi^{2}/dof')]:
        gr = makeGraph(getExpression(aout, expression))
        mps.append(MultiPlot(expression, title = '{0};Iteration;{0}'.format(title), histos = {expression:gr},
                       kind='g', drawLegend = False))
        mps[-1].DrawOption = 'alp'
        if args.online:
            monSvc.publishHistogram(folder_name, 
                                    makeGraphHisto(expression, title, getExpression(aout, expression)),
                                    runNumber=int(run), add=False)

    # 2 halves
    c.cd()

    for i in range(1,6):
        mps.append(MultiPlot('1', title = 'M{0};Iteration;Variation [mm]'.format(i),kind='g', hlines=[0,-1,1], rangeY = [-3, 3]))
        mps[-1].DrawOption = 'alp'
        mps[-1].Add(makeGraph(*getDofDeltaConvergence(aout, dof='Tx', alignable='Muon/M{0}/M{0}ASide'.format(i))), 'Tx_A',1)
        mps[-1].Add(makeGraph(*getDofDeltaConvergence(aout, dof='Tx', alignable='Muon/M{0}/M{0}CSide'.format(i))), 'Tx_C',-1)
        mps[-1].Add(makeGraph(*getDofDeltaConvergence(aout, dof='Ty', alignable='Muon/M{0}/M{0}ASide'.format(i))), 'Ty_A',2)
        mps[-1].Add(makeGraph(*getDofDeltaConvergence(aout, dof='Ty', alignable='Muon/M{0}/M{0}CSide'.format(i))), 'Ty_C',-2)

        if args.online:
            monSvc.publishHistogram(folder_name, makeGraphHisto('M{0}A_Tx'.format(i), 'M{0}A;Iteration;Variation Tx [mm]'.format(i), 
                                                                getDofDeltaConvergence(aout, dof='Tx', alignable='Muon/M{0}/M{0}ASide'.format(i))[0]), runNumber=int(run), add=False)
            monSvc.publishHistogram(folder_name, makeGraphHisto('M{0}C_Tx'.format(i), 'M{0}C;Iteration;Variation Tx [mm]'.format(i), 
                                                                getDofDeltaConvergence(aout, dof='Tx', alignable='Muon/M{0}/M{0}CSide'.format(i))[0]), runNumber=int(run), add=False)
            monSvc.publishHistogram(folder_name, makeGraphHisto('M{0}A_Ty'.format(i), 'M{0}A;Iteration;Variation Ty [mm]'.format(i), 
                                                                getDofDeltaConvergence(aout, dof='Ty', alignable='Muon/M{0}/M{0}ASide'.format(i))[0]), runNumber=int(run), add=False)
            monSvc.publishHistogram(folder_name, makeGraphHisto('M{0}C_Ty'.format(i), 'M{0}C;Iteration;Variation Ty [mm]'.format(i), 
                                                                getDofDeltaConvergence(aout, dof='Ty', alignable='Muon/M{0}/M{0}CSide'.format(i))[0]), runNumber=int(run), add=False)
    

    c1 = getDrawnCanvas(mps)
    c1.Print(outputFile_name)
    

        
    c.Print(outputFile_name+']')

    if args.online:
        gaudi.stop()
        gaudi.finalize()
    

