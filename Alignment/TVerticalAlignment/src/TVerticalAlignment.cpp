#include "TVerticalAlignment/TVerticalAlignment.h"
#include "boost/assign.hpp"


using namespace Alignment::TVerticalAlignment;
class OTNames;

TVerticalAlignment::TVerticalAlignment() 
{
}

void TVerticalAlignment::initParameter(std::string detector)
{
  if(detector=="OT"){
    std::pair<std::string, std::string> directory("Track/OTYAlign/ModuleLocYLHCbIDs/","Track/OTYAlign/ModuleLocYExp/");
    std::map<std::string,double> distance = boost::assign::map_list_of ("F", 36.) ("S1", 0) ("S23", 0) ("ALL",36);
    std::map<std::string,double> length   = boost::assign::map_list_of ("F", 36.) ("S1", 200) ("S23", 400) ("ALL",36);
    std::map<std::string,double> edge     = boost::assign::map_list_of ("F", 180.-50) ("S1", 120) ("S23", 80) ("ALL",180-50);
    std::map<std::string,double> step     = boost::assign::map_list_of ("F", 0.) ("S1", 0) ("S23", 11) ("ALL",0);
    double tolerance = 20;
    bool   mc  = false;
    double sigmaconstraint = 0.5;

    m_param[detector] = Param(OTParam(directory,distance,length,edge,step,tolerance,mc, sigmaconstraint));
  }
  else if(detector=="IT"){
    std::pair<std::string, std::string> directory("Track/ITYAlign/SectLocYLHCbIDs/","Track/ITYAlign/SectLocYExp/");
    double scale     = 30.;
    double tolerance = 3.0;
    double sigmaconstraint = 0.2;
    m_param[detector] = Param(STParam(directory,scale,tolerance,sigmaconstraint));
  }else if(detector=="TT"){
    std::pair<std::string, std::string> directory("Track/TTYAlign/SectLocYLHCbIDs/","Track/TTYAlign/SectLocYExp/");
    double scale     = 15.;
    double tolerance = 5.0;
    double sigmaconstraint = 0.2;
    m_param[detector] = Param(STParam(directory,scale,tolerance,sigmaconstraint));
  }
}

Param TVerticalAlignment::getParameter(std::string detector)
{
   auto it = m_param.find(detector);
   return it->second;
}

