// Include files

#ifndef OT2DPLOT_H
#define OT2DPLOT_H

#include <map>
#include <string>
#include "OTNames.h"
#include "TH2D.h"

using namespace std;

/** @class OT2DPlot OT2DPlot.h macros/OT2DPlot.h
 *   *
 *  @author Frederic Guillaume Dupertuis
 *  @date   2010-11-23
 */
class OT2DPlot {
public: 
  /// Standard constructor
  OT2DPlot() {}; 
  OT2DPlot(const char* name, const char* title, const char* plotType="Module");
  virtual ~OT2DPlot() {}; ///< Destructor

  void Draw(Option_t* option = "");
  void Fill(string const& module, double const& value, bool const& qcombined = false);
  TH2D* Histogram() {return m_hmap;}
protected:

private:
  std::map<int,double> m_XMap;
  std::map<int,double> m_YMap;
  OTNames* m_names;
  TString m_plotType;
  
  int    m_NBinX;
  int    m_NBinY;
  double m_LowX;
  double m_LowY;
  double m_UpX;
  double m_UpY;

  TH2D* m_hmap;

  void InitMaps();
  void PlotLabels();
  void PlotBoxes();
  //ClassDef(OT2DPlot,1);
};

#endif
