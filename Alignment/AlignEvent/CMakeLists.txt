################################################################################
# Package: AlignEvent
################################################################################
gaudi_subdir(AlignEvent v1r3)

gaudi_depends_on_subdirs(Alignment/AlignKernel
                         GaudiObjDesc
                         Kernel/LHCbKernel)

find_package(GSL)

include(GaudiObjDesc)

god_build_headers(xml/*.xml)

find_package(CLHEP)
find_package(ROOT)
include_directories(SYSTEM ${CLHEP_INCLUDE_DIRS} ${ROOT_INCLUDE_DIRS})

gaudi_add_library(AlignEvent
                  src/*.cpp
                  NO_PUBLIC_HEADERS
                  INCLUDE_DIRS GSL
                  LINK_LIBRARIES GSL AlignKernel LHCbKernel)

god_build_dictionary(xml/*.xml
                     INCLUDE_DIRS GSL
                     LINK_LIBRARIES GSL AlignKernel LHCbKernel AlignEvent)

