// From Gaudi
#include "GaudiAlg/GaudiTool.h"
#include "GaudiKernel/DeclareFactoryEntries.h"

// Interface
#include "AlignmentInterfaces/IWriteAlignmentConditionsTool.h"

// From Det
#include "DetDesc/Condition.h"

// From Rich
#include "RichDet/DeRichSystem.h"

// std
#include <sstream>
#include <fstream>
#include <boost/filesystem.hpp>

class WriteRichAlignmentsTool
  : public GaudiTool, virtual public IWriteAlignmentConditionsTool
{
public:
  /// constructor
  WriteRichAlignmentsTool( const std::string& type,
                           const std::string& name,
                           const IInterface* parent); ///< Standard constructor

  /// destructor
  ~WriteRichAlignmentsTool();

  /// initialize
  StatusCode initialize() override;

  // Everything configured via options
  StatusCode write() const override;

  // Everything configured via options
  StatusCode write(const std::string& version) const override;

private:
  /// Pointer to Rich Sys Detector Element
  const DeRichSystem * m_RichSys;

  StatusCode createXmlFile( const std::string& filename,
                            const std::string& contents,
                            const std::string& version) const;

  //StatusCode writeSingleXMLForAll(const std::string& version) const;
  StatusCode writeMirrAlign(const std::string& version) const; // Rich{1,2} Mirrors.xml
  StatusCode writeDetectorAlign(const std::string& version) const; // Rich{1,2} RichDetector.xml
  StatusCode writeMirrHpdAlign(const std::string& version) const; // Rich{1,2} HPDsP{0,1}.xml
  StatusCode writeMirrHpdPanelAlign(const std::string& version) const; // Rich{1,2} HPDPanels.xml

  int m_minHPDID;
  int m_maxHPDID;

  int m_NumSph1 = 4;  // RICH1 primary
  int m_NumSec1 = 16; // RICH1 secondary
  int m_NumSph2 = 56; // RICH2 primary
  int m_NumSec2 = 40; // RICH2 secondary

  //int m_firstHPD1 =   0;
  //int m_NumHPDs1  = 196;  // 196 HPDs in RICH1. Numbered 0-195 inclusive
  //int m_firstHPD2 = 196;
  //int m_NumHPDs2  = 288;  // 288 HPDs in RICH2. Numbered 196-483 inclusive

  std::string m_directory;
  unsigned int m_precision;
  std::string m_author;
  std::string m_description;
  std::string m_version;
  bool m_online;

};


/******************************************************************************/
/* Implementation                                                             */
/******************************************************************************/

DECLARE_TOOL_FACTORY( WriteRichAlignmentsTool )

WriteRichAlignmentsTool::WriteRichAlignmentsTool( const std::string& type,
                                                  const std::string& name,
                                                  const IInterface* parent)
: GaudiTool( type,name,parent)
{
  // interface
  declareInterface<IWriteAlignmentConditionsTool>(this);
  // properties
  declareProperty("minHPDID"   , m_minHPDID =   0 );
  declareProperty("maxHPDID"   , m_maxHPDID = 484 );
  declareProperty("NumSph1"     , m_NumSph1 =  4 );
  declareProperty("NumSec1"     , m_NumSec1 = 16 );
  declareProperty("NumSph2"     , m_NumSph2 = 56 );
  declareProperty("NumSec2"     , m_NumSec2 = 40 );
  //declareProperty("m_firstHPD1" , m_firstHPD1 = 0 );
  //declareProperty("m_NumHPDs1"  , m_NumHPDs1 = 196 );
  //declareProperty("m_firstHPD2" , m_firstHPD2 =  196 );
  //declareProperty("m_NumHPDs2"  , m_NumHPDs2 = 288 );
  declareProperty("Directory"   , m_directory = "xml" );
  declareProperty("Precision"   , m_precision = 8u);
  declareProperty("Author"      , m_author = "");
  declareProperty("Version"     , m_version = "");
  declareProperty("Description" , m_description = "");
  declareProperty("OnlineMode"  , m_online = false);
}

WriteRichAlignmentsTool::~WriteRichAlignmentsTool() {}

StatusCode WriteRichAlignmentsTool::initialize()
{
  StatusCode sc = GaudiTool::initialize(); // must be executed first
  if ( sc.isFailure() ) return sc;  // error printed already by GaudiAlgorithm

  /// Get pointer to detector
  m_RichSys = getDet<DeRichSystem>( DeRichLocations::RichSystem );

  return StatusCode::SUCCESS;
}


StatusCode WriteRichAlignmentsTool::write() const
{
  return write(m_version);
}


StatusCode WriteRichAlignmentsTool::write(const std::string& version) const
{
  StatusCode sc = StatusCode::SUCCESS;

//  sc = writeSingleXMLForAll(version);
  sc = writeMirrAlign(version);
  sc = writeDetectorAlign(version);
  sc = writeMirrHpdAlign(version);
  sc = writeMirrHpdPanelAlign(version);

  if ( sc.isFailure() ) return Warning( "Failed to write conditions to xml", StatusCode::FAILURE );
  return sc;
}

namespace {
  void stringreplace(std::string& astring,
                     const std::string& in,
                     const std::string& out,
                     size_t pos=0)
  {
    while( pos < astring.size() && (pos = astring.find(in,pos))!=std::string::npos) {
      astring.replace( pos, in.size(), out);
      pos += out.size();
    }
  }

  std::string formattedcondition( const Condition& condition, unsigned int precision )
  {
    std::ostringstream strstream;
    strstream << condition.toXml( "", false , precision ) << std::endl;
    std::string rc = strstream.str();

    stringreplace(rc,"condition>","condition>\n");

    return rc;
  }
}

StatusCode WriteRichAlignmentsTool::createXmlFile(const std::string& filename,
                                                  const std::string& version,
                                                  const std::string& contents) const
{
  /// create directory if it does not exist
  boost::filesystem::path apath(filename);
  boost::filesystem::create_directories(apath.parent_path());

  /// create the output file
  std::ofstream output( filename );
  if ( output.fail() ) {
    return Warning( "Failed to open output file " + filename, StatusCode::FAILURE );
  }

  // write the header
  if (!m_online) {
    output  << "<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>\n"
            << "<!DOCTYPE DDDB SYSTEM \"conddb:/DTD/structure.dtd\">\n";
    output  << "<DDDB>\n\n";
  }

  /// write the contents
  output << contents;

  /// write the footer
  if (!m_online) {
    output << "</DDDB>\n";
  }
  /// close the file
  output.close();

  return StatusCode::SUCCESS;
}

/*
//=============================================================================
//  Write Mirror align (entire thing into one XML)
//=============================================================================
StatusCode WriteRichAlignmentsTool::writeSingleXMLForAll(const std::string& version) const
{
  StatusCode sc = StatusCode::SUCCESS;

  const int totalRICHes = 2;
  std::string files[totalRICHes] =
    {
      "Rich1/Alignment/Mirrors.xml",
      "Rich2/Alignment/Mirrors.xml"
    };

  std::stringstream XMLs[totalRICHes];
  const int ForRich1 = 0;
  const int ForRich2 = 1;
  std::string subDet1 = "Rich1";
  std::string subDet2 = "Rich2";

  const int totalNonHpdCondTypes = 10;
  std::string condStarts[totalNonHpdCondTypes] =
	{
	  "SphMirror",          // m_NumSph1, numbered
	  "SecMirrorMasterTop", // 1, not numbered
	  "SecMirrorMasterBot", // 1, not numbered
	  "SecMirror",          // m_NumSec1, numbered
	  "Rich1",              // 1, not numbered
	  "Rich2SphMirrorCont", // 2, numbered
	  "Rich2SecMirrorCont", // 2, numbered
	  "Rich2SphMirror",     // m_NumSph2, numbered
	  "Rich2SecMirror"      // m_NumSec2, numbered
	  "Rich2",              // 1, not numbered
	};
  std::string condEnd = "_Align";

  // ==========================================
  // Go through all types of HPDalign constants
  // ==========================================

  // Rich1
  for(int HpdPanel1 = 0; HpdPanel1 < 2; HpdPanel1++ ) // HPDPanel // 2, numbered
  {
  	  std::string hpdPanelLoc1 = "/dd/Conditions/Alignment/"+subDet1+"/"+"HPDPanel"+std::to_string( HpdPanel1 )+condEnd;
	  if ( msgLevel(MSG::DEBUG) ) debug() << hpdPanelLoc1 << endmsg;
  	  Condition *myCond = get<Condition>( detSvc(), hpdPanelLoc1 ); // Get condition
  	  XMLs[ForRich1] << formattedcondition( *myCond, m_precision );
  }
  for(int Hpd1 = m_firstHPD1; Hpd1 < m_NumHPDs1+m_firstHPD1; Hpd1++ ) // HPD // m_NumHPDs1, numbered
  {
	  std::string hpdLoc1 = "/dd/Conditions/Alignment/"+subDet1+"/"+"HPD"+std::to_string( Hpd1 )+condEnd;
	  if ( msgLevel(MSG::DEBUG) ) debug() << hpdLoc1 << endmsg;
	  Condition *myCond = get<Condition>( detSvc(), hpdLoc1 ); // Get condition
	  XMLs[ForRich1] << formattedcondition( *myCond, m_precision );
  }

  // Rich2
  for(int HpdPanel2 = 0; HpdPanel2 < 2; HpdPanel2++ ) // HPDPanel // 2, numbered
  {
  	  std::string hpdPanelLoc2 = "/dd/Conditions/Alignment/"+subDet2+"/"+"HPDPanel"+std::to_string( HpdPanel2 )+condEnd;
	  if ( msgLevel(MSG::DEBUG) ) debug() << hpdPanelLoc2 << endmsg;
  	  Condition *myCond = get<Condition>( detSvc(), hpdPanelLoc2 ); // Get condition
  	  XMLs[ForRich2] << formattedcondition( *myCond, m_precision );
  }
  for(int Hpd2 = m_firstHPD2; Hpd2 < m_NumHPDs2+m_firstHPD2; Hpd2++ ) // HPD // m_NumHPDs2, numbered
  {
	  std::string hpdLoc2 = "/dd/Conditions/Alignment/"+subDet2+"/"+"HPD"+std::to_string( Hpd2 )+condEnd;
	  if ( msgLevel(MSG::DEBUG) ) debug() << hpdLoc2 << endmsg;
	  Condition *myCond = get<Condition>( detSvc(), hpdLoc2 ); // Get condition
	  XMLs[ForRich2] << formattedcondition( *myCond, m_precision );
  }

  // ==========================================
  // Go through all types of Mirror constants
  // ==========================================

  // Rich1
  for(int Sph1 = 0; Sph1 < m_NumSph1; Sph1++ )
  {
	  std::string alignLoc0 = "/dd/Conditions/Alignment/"+subDet1+"/"+condStarts[0]+std::to_string( Sph1 )+condEnd;
	  if ( msgLevel(MSG::DEBUG) ) debug() << alignLoc0 << endmsg;
	  Condition *myCond0 = get<Condition>( detSvc(), alignLoc0 ); // Get condition
	  XMLs[ForRich1] << formattedcondition( *myCond0, m_precision );
  }
  std::string alignLoc1 = "/dd/Conditions/Alignment/"+subDet1+"/"+condStarts[1]+condEnd;
  if ( msgLevel(MSG::DEBUG) ) debug() << alignLoc1 << endmsg;
  Condition *myCond1 = get<Condition>( detSvc(), alignLoc1 ); // Get condition
  XMLs[ForRich1] << formattedcondition( *myCond1, m_precision );
  std::string alignLoc2 = "/dd/Conditions/Alignment/"+subDet1+"/"+condStarts[2]+condEnd;
  if ( msgLevel(MSG::DEBUG) ) debug() << alignLoc2 << endmsg;
  Condition *myCond2 = get<Condition>( detSvc(), alignLoc2 ); // Get condition
  XMLs[ForRich1] << formattedcondition( *myCond2, m_precision );
  for(int Sec1 = 0; Sec1 < m_NumSec1; Sec1++ )
  {
	  std::string alignLoc3 = "/dd/Conditions/Alignment/"+subDet1+"/"+condStarts[3]+std::to_string( Sec1 )+condEnd;
	  if ( msgLevel(MSG::DEBUG) ) debug() << alignLoc3 << endmsg;
	  Condition *myCond3 = get<Condition>( detSvc(), alignLoc3 ); // Get condition
	  XMLs[ForRich1] << formattedcondition( *myCond3, m_precision );
  }
  std::string alignLocR1 = "/dd/Conditions/Alignment/"+subDet1+"/"+condStarts[4]+condEnd;
  if ( msgLevel(MSG::DEBUG) ) debug() << alignLocR1 << endmsg;
  Condition *myCondR1 = get<Condition>( detSvc(), alignLocR1 ); // Get condition
  XMLs[ForRich1] << formattedcondition( *myCondR1, m_precision );

  // Rich2
  for(int SphCont = 0; SphCont < 2; SphCont++ )
  {
  	  std::string alignLoc4 = "/dd/Conditions/Alignment/"+subDet2+"/"+condStarts[5]+std::to_string( SphCont )+condEnd;
	  if ( msgLevel(MSG::DEBUG) ) debug() << alignLoc4 << endmsg;
  	  Condition *myCond4 = get<Condition>( detSvc(), alignLoc4 ); // Get condition
  	  XMLs[ForRich2] << formattedcondition( *myCond4, m_precision );
  }
  for(int SecCont = 0; SecCont < 2; SecCont++ )
  {
  	  std::string alignLoc5 = "/dd/Conditions/Alignment/"+subDet2+"/"+condStarts[6]+std::to_string( SecCont )+condEnd;
	  if ( msgLevel(MSG::DEBUG) ) debug() << alignLoc5 << endmsg;
  	  Condition *myCond5 = get<Condition>( detSvc(), alignLoc5 ); // Get condition
  	  XMLs[ForRich2] << formattedcondition( *myCond5, m_precision );
  }
  for(int Sph2 = 0; Sph2 < m_NumSph2; Sph2++ )
  {
	  std::string alignLoc6 = "/dd/Conditions/Alignment/"+subDet2+"/"+condStarts[7]+std::to_string( Sph2 )+condEnd;
	  if ( msgLevel(MSG::DEBUG) ) debug() << alignLoc6 << endmsg;
	  Condition *myCond6 = get<Condition>( detSvc(), alignLoc6 ); // Get condition
	  XMLs[ForRich2] << formattedcondition( *myCond6, m_precision );
  }
  for(int Sec2 = 0; Sec2 < m_NumSec2; Sec2++ )
  {
	  std::string alignLoc7 = "/dd/Conditions/Alignment/"+subDet2+"/"+condStarts[8]+std::to_string( Sec2 )+condEnd;
	  if ( msgLevel(MSG::DEBUG) ) debug() << alignLoc7 << endmsg;
	  Condition *myCond7 = get<Condition>( detSvc(), alignLoc7 ); // Get condition
	  XMLs[ForRich2] << formattedcondition( *myCond7, m_precision );
  }
  std::string alignLocR2 = "/dd/Conditions/Alignment/"+subDet2+"/"+condStarts[9]+condEnd;
  if ( msgLevel(MSG::DEBUG) ) debug() << alignLocR2 << endmsg;
  Condition *myCondR2 = get<Condition>( detSvc(), alignLocR2 ); // Get condition
  XMLs[ForRich2] << formattedcondition( *myCondR2, m_precision );

  // ==========================================
  // Loop over RICHes
  // ==========================================
  std::string fileName;

  for( int rID=0; rID<totalRICHes; rID++ )
  {
    fileName = m_directory + "/" + files[rID];
    sc = createXmlFile( fileName, version, XMLs[rID].str() );
  }

  return sc;
}
*/

//=============================================================================
//  Write Mirror align only
//=============================================================================
StatusCode WriteRichAlignmentsTool::writeMirrAlign(const std::string& version) const
{
  StatusCode sc = StatusCode::SUCCESS;

  const int totalRICHes = 2;
  std::string files[totalRICHes] =
    {
      "Rich1/Alignment/Mirrors.xml",
      "Rich2/Alignment/Mirrors.xml"
    };

  std::stringstream XMLs[totalRICHes];
  const int ForRich1 = 0;
  const int ForRich2 = 1;
  std::string subDet1 = "Rich1";
  std::string subDet2 = "Rich2";

  const int totalMirrCondTypes = 10;
  std::string condStarts[totalMirrCondTypes] =
	{
	  "SphMirror",          // m_NumSph1, numbered*
	  "SecMirrorMasterTop", // 1, not numbered
	  "SecMirrorMasterBot", // 1, not numbered
	  "SecMirror",          // m_NumSec1, numbered*
	  "Rich2SphMirrorCont", // 2, numbered
	  "Rich2SecMirrorCont", // 2, numbered
	  "Rich2SphMirror",     // m_NumSph2, numbered*
	  "Rich2SecMirror"      // m_NumSec2, numbered*
	};
  std::string condEnd = "_Align";

  // ==========================================
  // Go through all types of Mirror constants
  // ==========================================

  // Rich1
  for(int Sph1 = 0; Sph1 < m_NumSph1; Sph1++ )
  {
	  std::string alignLoc0 = "/dd/Conditions/Alignment/"+subDet1+"/"+condStarts[0]+std::to_string( Sph1 )+condEnd;
	  if ( msgLevel(MSG::DEBUG) ) debug() << alignLoc0 << endmsg;
	  Condition *myCond0 = get<Condition>( detSvc(), alignLoc0 ); // Get condition
	  XMLs[ForRich1] << formattedcondition( *myCond0, m_precision );
  }
  std::string alignLoc1 = "/dd/Conditions/Alignment/"+subDet1+"/"+condStarts[1]+condEnd;
  if ( msgLevel(MSG::DEBUG) ) debug() << alignLoc1 << endmsg;
  Condition *myCond1 = get<Condition>( detSvc(), alignLoc1 ); // Get condition
  XMLs[ForRich1] << formattedcondition( *myCond1, m_precision );
  std::string alignLoc2 = "/dd/Conditions/Alignment/"+subDet1+"/"+condStarts[2]+condEnd;
  if ( msgLevel(MSG::DEBUG) ) debug() << alignLoc2 << endmsg;
  Condition *myCond2 = get<Condition>( detSvc(), alignLoc2 ); // Get condition
  XMLs[ForRich1] << formattedcondition( *myCond2, m_precision );
  for(int Sec1 = 0; Sec1 < m_NumSec1; Sec1++ )
  {
	  std::string alignLoc3 = "/dd/Conditions/Alignment/"+subDet1+"/"+condStarts[3]+std::to_string( Sec1 )+condEnd;
	  if ( msgLevel(MSG::DEBUG) ) debug() << alignLoc3 << endmsg;
	  Condition *myCond3 = get<Condition>( detSvc(), alignLoc3 ); // Get condition
	  XMLs[ForRich1] << formattedcondition( *myCond3, m_precision );
  }

  // Rich2
  for(int SphCont = 0; SphCont < 2; SphCont++ )
  {
  	  std::string alignLoc4 = "/dd/Conditions/Alignment/"+subDet2+"/"+condStarts[4]+std::to_string( SphCont )+condEnd;
	  if ( msgLevel(MSG::DEBUG) ) debug() << alignLoc4 << endmsg;
  	  Condition *myCond4 = get<Condition>( detSvc(), alignLoc4 ); // Get condition
  	  XMLs[ForRich2] << formattedcondition( *myCond4, m_precision );
  }
  for(int SecCont = 0; SecCont < 2; SecCont++ )
  {
  	  std::string alignLoc5 = "/dd/Conditions/Alignment/"+subDet2+"/"+condStarts[5]+std::to_string( SecCont )+condEnd;
	  if ( msgLevel(MSG::DEBUG) ) debug() << alignLoc5 << endmsg;
  	  Condition *myCond5 = get<Condition>( detSvc(), alignLoc5 ); // Get condition
  	  XMLs[ForRich2] << formattedcondition( *myCond5, m_precision );
  }
  for(int Sph2 = 0; Sph2 < m_NumSph2; Sph2++ )
  {
	  std::string alignLoc6 = "/dd/Conditions/Alignment/"+subDet2+"/"+condStarts[6]+std::to_string( Sph2 )+condEnd;
	  if ( msgLevel(MSG::DEBUG) ) debug() << alignLoc6 << endmsg;
	  Condition *myCond6 = get<Condition>( detSvc(), alignLoc6 ); // Get condition
	  XMLs[ForRich2] << formattedcondition( *myCond6, m_precision );
  }
  for(int Sec2 = 0; Sec2 < m_NumSec2; Sec2++ )
  {
	  std::string alignLoc7 = "/dd/Conditions/Alignment/"+subDet2+"/"+condStarts[7]+std::to_string( Sec2 )+condEnd;
	  if ( msgLevel(MSG::DEBUG) ) debug() << alignLoc7 << endmsg;
	  Condition *myCond7 = get<Condition>( detSvc(), alignLoc7 ); // Get condition
	  XMLs[ForRich2] << formattedcondition( *myCond7, m_precision );
  }

  // ==========================================
  // Loop over RICHes
  // ==========================================
  std::string fileName;

  for( int rID=0; rID<totalRICHes; rID++ )
  {
    fileName = m_directory + "/" + files[rID];
    sc = createXmlFile( fileName, version, XMLs[rID].str() );
  }

  return sc;
}

//=============================================================================
//  Write Detector align only
//=============================================================================
StatusCode WriteRichAlignmentsTool::writeDetectorAlign(const std::string& version) const
{
  StatusCode sc = StatusCode::SUCCESS;

  const int totalRICHes = 2;
  std::string files[totalRICHes] =
    {
      "Rich1/Alignment/RichDetector.xml",
      "Rich2/Alignment/RichDetector.xml"
    };

  std::stringstream XMLs[totalRICHes];
  const int ForRich1 = 0;
  const int ForRich2 = 1;
  std::string subDet1 = "Rich1";
  std::string subDet2 = "Rich2";

  const int totalDetCondTypes = 10;
  std::string condStarts[totalDetCondTypes] =
	{
	  "Rich1",              // 1, not numbered
	  "Rich2",              // 1, not numbered
	};
  std::string condEnd = "_Align";

  // ==========================================
  // Go through all types of Mirror constants
  // ==========================================

  // Rich1
  std::string alignLocR1 = "/dd/Conditions/Alignment/"+subDet1+"/"+condStarts[0]+condEnd;
  if ( msgLevel(MSG::DEBUG) ) debug() << alignLocR1 << endmsg;
  Condition *myCondR1 = get<Condition>( detSvc(), alignLocR1 ); // Get condition
  XMLs[ForRich1] << formattedcondition( *myCondR1, m_precision );

  // Rich2
  std::string alignLocR2 = "/dd/Conditions/Alignment/"+subDet2+"/"+condStarts[1]+condEnd;
  if ( msgLevel(MSG::DEBUG) ) debug() << alignLocR2 << endmsg;
  Condition *myCondR2 = get<Condition>( detSvc(), alignLocR2 ); // Get condition
  XMLs[ForRich2] << formattedcondition( *myCondR2, m_precision );

  // ==========================================
  // Loop over RICHes
  // ==========================================
  std::string fileName;

  for( int rID=0; rID<totalRICHes; rID++ )
  {
    fileName = m_directory + "/" + files[rID];
    sc = createXmlFile( fileName, version, XMLs[rID].str() );
  }

  return sc;
}

//=============================================================================
//  Write MirrorHPD align only
//=============================================================================
StatusCode WriteRichAlignmentsTool::writeMirrHpdAlign(const std::string& version) const
{
  StatusCode sc = StatusCode::SUCCESS;

  const int totalPanels = 4;

  std::string files[totalPanels] =
    {
      "Rich1/Alignment/HPDsP0.xml",
      "Rich1/Alignment/HPDsP1.xml",
      "Rich2/Alignment/HPDsP0.xml",
      "Rich2/Alignment/HPDsP1.xml"
    };

  std::stringstream RichHpdAlign[totalPanels];

  // ==========================================
  // Loop over HPDs
  // ==========================================
  for(int hpdID = m_minHPDID; hpdID< m_maxHPDID; ++hpdID)
  {
    // HPD copy number
    const Rich::DAQ::HPDCopyNumber copyNumber =  Rich::DAQ::HPDCopyNumber( hpdID );

    // SmartID
    const LHCb::RichSmartID smartID = m_RichSys->richSmartID( copyNumber );

    if ( msgLevel(MSG::DEBUG) ) debug() << "HPD ID " << hpdID
                                        << ", smartID "  << smartID.toString()
                                        << ", smartID.key() " << smartID.key()
                                        << endmsg;

    // Get condition
    std::string subDet;
    if( smartID.rich() == Rich::DetectorType::Rich1 ) subDet = "Rich1";
    else subDet = "Rich2";

    std::string type = "HPD";

    std::string alignLoc = "/dd/Conditions/Alignment/"+subDet+"/"+type+std::to_string( copyNumber.data() )+"_Align";

    Condition *myCond = get<Condition>( detSvc(), alignLoc );

    // Rich1 P0
    if( smartID.rich()     == Rich::DetectorType::Rich1
        && smartID.panel() == Rich::Side::top )
    {
      RichHpdAlign[0] << formattedcondition( *myCond, m_precision );
    }

    // Rich1 P1
    if( smartID.rich()     == Rich::DetectorType::Rich1
        && smartID.panel() == Rich::Side::bottom )
    {
      RichHpdAlign[1] << formattedcondition( *myCond, m_precision );
    }

    // Rich2 P0
    if( smartID.rich()     == Rich::DetectorType::Rich2
        && smartID.panel() == Rich::Side::left )
    {
      RichHpdAlign[2] << formattedcondition( *myCond, m_precision );
    }

    // Rich2 P1
    if( smartID.rich()     == Rich::DetectorType::Rich2
        && smartID.panel() == Rich::Side::right )
    {
      RichHpdAlign[3] << formattedcondition( *myCond, m_precision );
    }

  } // Loop over HPDs

  // ==========================================
  // Loop over Panels
  // ==========================================
  std::string fileName;

  for( int pID=0; pID<totalPanels; pID++ )
  {
    fileName = m_directory + "/" + files[pID];
    sc = createXmlFile( fileName, version, RichHpdAlign[pID].str() );
  }

  return sc;
}


//=============================================================================
//  Write MirrorHPD panel align only
//=============================================================================
StatusCode WriteRichAlignmentsTool::writeMirrHpdPanelAlign(const std::string& version) const
{
  StatusCode sc = StatusCode::SUCCESS;

  const int totalRICHes = 2;
  std::string files[totalRICHes] =
    {
      "Rich1/Alignment/HPDPanels.xml",
      "Rich2/Alignment/HPDPanels.xml"
    };

  std::stringstream XMLs[totalRICHes];
  const int ForRich1 = 0;
  const int ForRich2 = 1;
  std::string subDet1 = "Rich1";
  std::string subDet2 = "Rich2";

  std::string condEnd = "_Align";

  // ==========================================
  // Go through all types of HPDalign constants
  // ==========================================

  // Rich1
  for(int HpdPanel1 = 0; HpdPanel1 < 2; HpdPanel1++ ) // HPDPanel // 2, numbered
  {
  	  std::string hpdPanelLoc1 = "/dd/Conditions/Alignment/"+subDet1+"/"+"HPDPanel"+std::to_string( HpdPanel1 )+condEnd;
	  if ( msgLevel(MSG::DEBUG) ) debug() << hpdPanelLoc1 << endmsg;
  	  Condition *myCond = get<Condition>( detSvc(), hpdPanelLoc1 ); // Get condition
  	  XMLs[ForRich1] << formattedcondition( *myCond, m_precision );
  }

  // Rich2
  for(int HpdPanel2 = 0; HpdPanel2 < 2; HpdPanel2++ ) // HPDPanel // 2, numbered
  {
  	  std::string hpdPanelLoc2 = "/dd/Conditions/Alignment/"+subDet2+"/"+"HPDPanel"+std::to_string( HpdPanel2 )+condEnd;
	  if ( msgLevel(MSG::DEBUG) ) debug() << hpdPanelLoc2 << endmsg;
  	  Condition *myCond = get<Condition>( detSvc(), hpdPanelLoc2 ); // Get condition
  	  XMLs[ForRich2] << formattedcondition( *myCond, m_precision );
  }

  // ==========================================
  // Loop over RICHes
  // ==========================================
  std::string fileName;

  for( int rID=0; rID<totalRICHes; rID++ )
  {
    fileName = m_directory + "/" + files[rID];
    sc = createXmlFile( fileName, version, XMLs[rID].str() );
  }

  return sc;
}
