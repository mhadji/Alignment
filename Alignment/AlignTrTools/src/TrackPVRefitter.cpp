#include "GaudiKernel/AlgFactory.h"
#include "GaudiKernel/ToolHandle.h"
#include "GaudiKernel/PhysicalConstants.h"
#include "GaudiAlg/GaudiAlgorithm.h"
#include "Event/Track.h"
#include "Event/RecVertex.h"
#include "TrackInterfaces/ITrackVertexer.h"
#include "TrackInterfaces/ITrackFitter.h"
#include "TrackInterfaces/ITrackSelector.h"
#include <algorithm>

class TrackPVRefitter : public GaudiAlgorithm
{
public:

   /** Standard construtor */
  TrackPVRefitter( const std::string& name, ISvcLocator* pSvcLocator );

  /** Algorithm initialize */
  StatusCode initialize() override;

  /** Algorithm finalize */
  StatusCode finalize() override;

  /** Algorithm execute */
  StatusCode execute() override;

private:
  std::string m_trackContainerName;
  std::string m_pvContainerName;
  double m_maxLongTrackChisqPerDof ;

  ToolHandle<ITrackVertexer> m_vertexer ;
  ToolHandle<ITrackFitter> m_trackfitter ;
  ToolHandle<ITrackSelector> m_trackselector ;
} ;

// Declaration of the Algorithm Factory
DECLARE_ALGORITHM_FACTORY( TrackPVRefitter )

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
TrackPVRefitter::TrackPVRefitter( const std::string& name,
					ISvcLocator* pSvcLocator)
  : GaudiAlgorithm( name , pSvcLocator ),
    m_vertexer("TrackVertexer",this),
    m_trackfitter("",this),
    m_trackselector("",this)
{
  declareProperty( "PVContainer", m_pvContainerName = LHCb::RecVertexLocation::Primary ) ;
  declareProperty( "TrackFitter", m_trackfitter ) ;
  declareProperty( "TrackSelector", m_trackselector ) ;
  //declareProperty( "MaxLongTrackChisqPerDof", m_maxLongTrackChisqPerDof = 5 ) ;
}

//=============================================================================
// Initialization
//=============================================================================
StatusCode TrackPVRefitter::initialize()
{
  StatusCode sc = GaudiAlgorithm::initialize(); // must be executed first
  if ( sc.isFailure() ) return sc;              // error printed already by GaudiAlgorithm
  sc = m_vertexer.retrieve() ;
  if( !m_trackfitter.empty() ) sc = m_trackfitter.retrieve() ;
  if( !m_trackselector.empty() ) sc = m_trackselector.retrieve() ;
  return sc;
}

//=============================================================================
// Initialization
//=============================================================================
StatusCode TrackPVRefitter::finalize()
{
  m_vertexer.release().ignore() ;
  if( !m_trackfitter.empty() ) m_trackfitter.release().ignore() ;
  if( !m_trackselector.empty() ) m_trackselector.release().ignore() ;
  return GaudiAlgorithm::finalize() ;
}

namespace {
  std::vector<const LHCb::Track*> myconvert( const SmartRefVector<LHCb::Track> & tracks )
  {
    std::vector<const LHCb::Track*> rc ; rc.reserve( tracks.size() ) ;
    std::copy_if( tracks.begin(), tracks.end(),
                  std::back_inserter(rc),
                  [](const LHCb::Track* t) { return t!=nullptr; } );
    return rc ;
  }
}

StatusCode TrackPVRefitter::execute()
{
  // for now I'll just create the track lists from the Best container
  for( const auto& pv : *get<LHCb::RecVertices>( m_pvContainerName )) {
    auto tracks = myconvert(pv->tracks()) ;

    // we select the tracks before we fit them, to save time
    if( !m_trackselector.empty() ) {
      tracks.erase( std::remove_if( tracks.begin(), tracks.end(),
                                    [&](const LHCb::Track* t)
                                    { return !m_trackselector->accept(*t); } ),
                    tracks.end() );
    }

    if( !m_trackfitter.empty() ) {
      std::transform( tracks.begin(), tracks.end(), tracks.begin(),
                      [&](const LHCb::Track* t) {
	        StatusCode sc = m_trackfitter->fit( const_cast<LHCb::Track&>(*t) );
	        return  sc.isSuccess() ? t : nullptr;
      });
      tracks.erase( std::remove( tracks.begin(), tracks.end(), nullptr ),
                    tracks.end() );
    }

    if( tracks.size() >= 2 ) {
      auto vertex = m_vertexer->fit( tracks ) ;
      vertex->setTechnique( pv->technique() ) ;
      *pv = *vertex ;
    }
  }

  return StatusCode::SUCCESS ;
}
